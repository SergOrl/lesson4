package by.itacademy.lesson4.array;

public class OutputableArray {
    private int[] array;

    public OutputableArray(int[] array) {
        this.array = array;

    }

    public void output() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        StringBuilder internal = new StringBuilder("by.itacademy.lesson4.array.ReadableArray");

        internal.append('[');
        for (int i = 0; i < array.length - 1; i++) {
            internal.append(array[i]).append(", ");
        }
        internal.append(array[array.length - 1]).append(']');

        return internal.toString();
    }
}