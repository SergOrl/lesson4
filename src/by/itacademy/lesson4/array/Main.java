package by.itacademy.lesson4.array;

public class Main {
    public static void main(String[] args) {
        int[] array = {5, 4, 3, 2, 1};

        ReadableArray readableArray = new ReadableArray(array);
        readableArray.input();

        array = readableArray.array();

        SortableArray sortableArray = new SortableArray(array);
        sortableArray.sort();

        array = sortableArray.array();

        OutputableArray outputableArray = new OutputableArray(array);
        outputableArray.output();
    }
}