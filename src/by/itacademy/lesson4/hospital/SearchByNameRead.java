package by.itacademy.lesson4.hospital;

import java.util.Scanner;

public class SearchByNameRead {
    private Scanner scanner = new Scanner(System.in);

    public String execute() {
        System.out.println("Введите имя пациента:");
        return scanner.next();
    }
}
