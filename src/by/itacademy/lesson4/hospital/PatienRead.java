package by.itacademy.lesson4.hospital;
import java.util.Scanner;

public class PatienRead {
    private Scanner scanner = new Scanner(System.in);

    public Patient execute() {
        System.out.println("Введите данные пациента!");

        String name = scanner.next();
        System.out.println("Введите имя пациента:");
        String LastName = scanner.next();
        System.out.println("Введите фамилию пациента:");
        String middleName = scanner.next();
        System.out.println("Введите отчество пациента:");
        int age = scanner.nextInt();
        System.out.println("Введите возраст пациента:");
        boolean healthy = scanner.nextBoolean();
        System.out.println("Болен ли:");

        return new Patient(new FIO(name, LastName, middleName), age, healthy);
    }
}
