package by.itacademy.lesson4.hospital;

public class FIO {
    private String name;
    private String LastName;
    private String middleName;

    public FIO(String name, String lastName, String middleName){
        this.name = name;
        this.LastName = lastName;
        this.middleName = middleName;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "FIO{" +
                "name='" + name + '\'' +
                ", LastName='" + LastName + '\'' +
                ", middleName='" + middleName + '\'' +
                '}';
    }
}

